import anime from 'animejs';
import { state } from "./state";

const duration = 200;
const tile_duration = 300;

export class state_6  {

    constructor () {
        this.prec_value = 0.1;

        this.animation_bg = anime({
            targets: '.bg_state_6',
            opacity: 1,
            scale: 1.1,
            duration: 200,
            autoplay: false
        });
    }

    run (analysers) {
        return new Promise((resolve, reject) => {
            const analyser = analysers.left;

            let buffer_length = analyser.frequencyBinCount;
            //console.log(buffer_length);
            let data_array = new Float32Array(buffer_length);
            analyser.getFloatFrequencyData(data_array);

            let max = Math.max(...data_array) + 60;
            //console.log('Max ', max);
            let seek_index = (Math.max(max, 0.1) / 315) * duration;
            this.animation_bg.seek(seek_index);
            resolve();
        });


    }

    enter () {
        console.log('Enter State 6');
        anime({
            targets: '.bg_state_5',
            opacity: 0,
            scale: 1,
            duration: 1000,
            elasticity: 200,
            easing: "easeInElastic"
        });
    }

    exit () {
        anime({
            targets: '.bg_state_6',
            opacity: 0,
            scale: 1
        });

        anime({
            targets: '.bg_state_5',
            opacity: 0,
            scale: 1,
            duration: 2000,
            elasticity: 200,
            easing: "easeInQuad"
        });
    }
}
