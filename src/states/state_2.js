import anime from 'animejs';
import { state } from "./state";

const duration = 200;

export class state_2  {

    constructor () {
        this.prec_value = 0.1;

        this.animation_bg = anime({
            targets: '.bg_state_2',
            opacity: 0.2,
            scale: 1.005,
            duration: 200,
            autoplay: false,
            loop: true
        })
    }

    construct_element() {
        this.timeline = anime.timeline();
        this.timeline.add({
            targets: '#state_2_tile_1',
            opacity: 1,
            scale: 1.2,
            duration: 4000,
            offset: 0,
            easing: 'easeInOutElastic',
            loop: false
        })
        .add({
            targets: '#state_2_tile_2',
            opacity: 1,
            duration: 4000,
            scale: 1.2,
            offset: 1500,
            easing: 'easeInOutElastic',
            loop: false
            
        })
        .add({
            targets: '#state_2_tile_3',
            opacity: 1,
            duration: 4000,
            scale: 1.2,
            offset: 2750,
            easing: 'easeInOutElastic',
            loop: false
        })
        .add({
            targets: '#state_2_tile_4',
            opacity: 1,
            duration: 4000,
            scale: 1.2,
            offset: 4250,
            easing: 'easeInOutElastic',
            loop: false
        })
        .add({
            targets: '#state_2_tile_5',
            opacity: 1,
            duration: 5000,
            scale: 1.2,
            offset: 5360,
            easing: 'easeInOutElastic',
            loop: false
        });
    }

    run (analysers) {

        return new Promise((resolve, reject) => {
            const analyser = analysers.left;

            let buffer_length = analyser.frequencyBinCount;

            let data_array = new Float32Array(buffer_length);
            analyser.getFloatFrequencyData(data_array);

            let max = Math.max(...data_array) + 60;

            let seek_index = (Math.max(max, 0.1) / 315) * duration;
            this.animation_bg.seek(seek_index);
            resolve();
        });
 
    }

    enter () {
        console.log('Enter State 2');
        anime({
            targets: '.bg_state_1',
            opacity: 0.5,
            scale: 1,
            duration: 1000,
            elasticity: 200,
            easing: "easeInElastic"
        });
        this.construct_element();
    }

    exit () {
        this.timeline.pause;
        anime({
            targets: '.bg_state_2',
            opacity: 0,
            scale: 1
        });
        
        anime({
            targets: '.tile_state_2',
            opacity: 0,
            scale: 1
        });

        anime({
            targets: '.bg_state_1',
            opacity: 0,
            scale: 1,
            easing: "easeInQuad"
        });
    }
}
