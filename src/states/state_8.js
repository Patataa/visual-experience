import anime from 'animejs';
import { state } from "./state";

const duration = 200;
const tile_duration = 300;

export class state_8  {

    constructor () {
        this.prec_value = 0.1;

        this.animation_bg = anime.timeline({
            targets: '.bg_state_8',
            autoplay: false,
            loop: false});

        this.animation_bg.add({
            opacity: 1,
            duration: 1500,
            scale: 1.2,
            easing: "easeOutCirc"
        });
    }

    run (analysers) {
        return new Promise((resolve, reject) => {

            const analyser = analysers.left;

            resolve();
        });


    }

    enter () {
    
        this.animation_bg.play();

    }

    exit () {
        this.animation_bg.reverse();
        anime({
            targets: '.bg_state_8',
            opacity: 0,
            scale: 1,
            duration: 1500
        });
    }
}
