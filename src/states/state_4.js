import anime from 'animejs';
import { state } from "./state";

const duration = 200;

export class state_4  {


    constructor () {
        this.prec_value = 0.1;

        this.animation_bg = anime({
            targets: '.bg_state_4',
            opacity: 0,
            scale: 1.3,
            duration: 200,
            autoplay: false
        });
        this.timeline = {};
    }


    construct_element() {
        this.timeline = anime.timeline({
            targets: '.tile_state_4',
            delay: function(el, i) { return i * 500 },
            duration: 5000,
            easing: 'easeOutExpo',
            autoplay: true,
            loop: false
          });

        this.timeline.add({
            opacity: 1,
            offset: 500,
            scale: 1.2,
        });
        
    }

    run (analysers) {
        return new Promise((resolve, reject) => {

            const analyser = analysers.left;

            let buffer_length = analyser.frequencyBinCount;
            let data_array = new Float32Array(buffer_length);
            analyser.getFloatFrequencyData(data_array);

            let max = Math.max(...data_array) + 60;
            this.seek_index = (Math.max(max, 0.1) / 315) * this.duration;
            this.animation_bg.seek(this.prec_value);

            this.prec_value = max;
            resolve();
        });

    }

    enter () {
        anime({
            targets: '.tile_state_3',
            opacity: 0
        });
        console.log('Enter State 4');
        this.construct_element();
    }

    exit () {
        this.timeline.pause();
        anime({
            targets: '.tile_state_4',
            duration: 150,
            easing: 'easeOutExpo',
            autoplay: true,
            opacity: 0,
            duration: 300,
            scale: 1
          });
   anime({
            targets: '.bg_state_4',
            opacity: 0,
            scale: 1
        });
    }
}
