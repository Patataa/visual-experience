import anime from 'animejs';
import { state } from "./state";

const duration = 200;
const tile_duration = 300;

export class state_5  {

    constructor () {
        this.prec_value = 0.1;

        this.animation_bg = anime({
            targets: '.bg_state_5',
            opacity: 1,
            scale: 1.1,
            duration: 200,
            autoplay: false
        });

        this.animations = [];

        let elements = document.getElementsByClassName('tile_state_5');
        console.log('elements');

        Array.from(document.getElementsByClassName('tile_state_5'))
            .forEach((item) => {
                console.log(item.id);
                let anim = anime({
                    targets: '#' + item.id,
                    opacity: 1,
                    scale: 1.3,
                    duration: tile_duration,
                    autoplay: false,
                    loop: true
                });

                this.animations.push(anim);
        });
    }

    run (analysers) {
        return new Promise((resolve, reject) => {

            const analyser = analysers.left;

            let buffer_length = analyser.frequencyBinCount;
            let data_array = new Float32Array(buffer_length);
            analyser.getFloatFrequencyData(data_array);

            let max = Math.max(...data_array) + 60;

            let bg_ratio = Math.max(max, 0.1) / 315;

            let seek_index = bg_ratio  * duration;

            this.animation_bg.seek(seek_index);


            // Define tiles index
            let data_parts_size = parseInt(buffer_length / (this.animations.length));
            for (let i = 0; i < this.animations.length; i++) {
                let reduce = 0;

                for (let j = 0; j < data_parts_size; j++) {
                    let data_index = i * this.animations.length + j;

                    let value = Math.max(data_array[i* (this.animations.length - 3) + j] / 2 + 80, 0.1); 

                    reduce += isNaN(value)? 0.1: value;
                }

                reduce /= data_parts_size;

                let tile_seek_index = Math.max(reduce, 0.1) * 0.7 + 0.3 * bg_ratio * tile_duration;

                tile_seek_index = Math.min(tile_seek_index, tile_duration);

                //console.log('i ', i, '  ', tile_seek_index);
                this.animations[i].seek( tile_seek_index);
            }

            resolve();
        });


    }

    enter () {
        console.log('Enter State 2');
        anime({
            targets: '.bg_state_4',
            opacity: 0,
            scale: 1,
            duration: 1000,
            elasticity: 200,
            easing: "easeInElastic"
        });

        anime({
            targets: '.tile_state_5',
            opacity: 0
        });
        anime({
            targets: '.tile_state_4',
            opacity: 0
        });
    }

    exit () {
        anime({
            targets: '.bg_state_5',
            opacity: 0,
            scale: 1
        });

        anime({
            targets: '.bg_state_4',
            opacity: 0,
            scale: 1,
            duration: 2000,
            elasticity: 200,
            easing: "easeInQuad"
        });


        anime({
            targets: '.tile_state_5',
            opacity: 0
        })
    }
}
