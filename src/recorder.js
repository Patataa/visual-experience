export default function record_config() {
  return navigator.mediaDevices.getUserMedia({
    video: false,
    audio: true
  }).then((stream) => {
      
    console.log(stream);
    const audioContext = new AudioContext();
    
    let microStream = audioContext.createMediaStreamSource(stream);

    // Split the stereo channels
    let splitter = audioContext.createChannelSplitter(2);
    
    microStream.connect(splitter);

    let analyser_1 = audioContext.createAnalyser();
    analyser_1.fftSize = 64;
    splitter.connect(analyser_1, 0);

    let analyser_2 = audioContext.createAnalyser();
    analyser_2.fftSize = 64;
    splitter.connect(analyser_2, 1);

    return {
        left: analyser_1,
        right: analyser_2
    }
  }).catch(function(err) {
    console.log("error", err);
  });;
}