import record_config from './recorder';
import io from 'socket.io-client';
const socket = io('http://localhost:3000');

const update = (analysers) => {
  //socket.emit('left_data', data.left);
  //socket.emit('right_data', data.right);

  setInterval(() => {
    const analyser = analysers.left;
    let buffer_length = analyser.frequencyBinCount;
    let data_array = new Float32Array(buffer_length);
    analyser.getFloatFrequencyData(data_array);
    let max = Math.max(...data_array) + 60;
    socket.emit('left_data', String(max));
  }, 50);
};

record_config()
  .then((analyzers)=> {
    update (analyzers);
});

