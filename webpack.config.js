const path = require('path');

module.exports = {
    entry: './src/index.js',
    devtool: 'inline-source-map',
    resolve: {
        extensions: ['.js' ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: path.join(__dirname),
        compress: true,
        port: 80
    }
};